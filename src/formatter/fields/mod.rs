/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#[derive(Debug)]
pub struct Names {
	pub time: String,
	pub level: String,
	pub message: String,
	pub context: String,
}

impl Default for Names {
	fn default() -> Self {
		Names {
			time: "time".into(),
			level: "level".into(),
			message: "message".into(),
			context: "context".into(),
		}
	}
}

pub trait Naming {
	fn set_field_names(&mut self, _names: Names) -> &mut Self
		where
			Self: Sized,
	{
		self
	}

	fn with_field_names(mut self, names: Names) -> Self
		where
			Self: Sized,
	{
		self.set_field_names(names);
		self
	}
}
