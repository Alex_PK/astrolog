/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use serde_json::{to_string as json_encode, Map as Object};

use crate::formatter::{context, date, level, Formats, RecordFormatter, fields};
use crate::logger::Record;

#[derive(Debug)]
pub struct Json {
	formats: Formats,
}

impl Json {
	pub fn new() -> Json {
		Json {
			formats: Formats::default(),
		}
	}
}

impl RecordFormatter for Json {
	fn format(&self, record: &Record) -> String {
		let mut res = Object::new();

		res.insert(self.formats.fields.time.clone(), self.formats.date(&record.dt).into());
		res.insert(
			self.formats.fields.level.clone(),
			self.formats.level(&record.level).into(),
		);
		res.insert(self.formats.fields.message.clone(), record.msg.clone().into());
		res.insert(self.formats.fields.context.clone(), record.context.to_value());

		json_encode(&res).unwrap_or_default()
	}
}

impl date::Formatting for Json {
	fn set_date_format(&mut self, date_format: date::Format) -> &mut Self {
		self.formats.set_date_format(date_format);
		self
	}
}

impl level::Formatting for Json {
	fn set_level_format(&mut self, level_format: level::Format) -> &mut Self {
		self.formats.set_level_format(level_format);
		self
	}
}

impl context::Formatting for Json {
	fn set_context_formatter(&mut self, _: Box<context::Formatter + Send + Sync + 'static>) -> &mut Self {
		self
	}
}

impl fields::Naming for Json {
	fn set_field_names(&mut self, names: fields::Names) -> &mut Self {
		self.formats.set_field_names(names);
		self
	}
}

#[cfg(test)]
mod tests {
	use super::super::super::Level;
	use super::*;

	#[test]
	fn basic_conversion() {
		let record = Record::new(Level::Debug, "Test").with("key", "value");

		let fmt = Json::new();
		assert_eq!(
			"{\"context\":{\"key\":\"value\"},\"level\":\"DEBG\",\"message\":\"Test\",\"time\":",
			&fmt.format(&record)[..66]
		);
	}
}
