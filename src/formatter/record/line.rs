/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use crate::formatter::{context, date, level, Formats, RecordFormatter, RecordReplacer, PLACEHOLDER_RE};
use crate::logger::Record;

#[derive(Debug)]
pub struct Line {
	template: String,
	formats: Formats,
}

impl Line {
	pub const DEFAULT_TEMPLATE: &'static str = "{{ [datetime]+ }}{{ level }}: {{ message }}{{ +context? }}\n";

	pub fn new() -> Line {
		Line {
			template: Line::DEFAULT_TEMPLATE.into(),
			formats: Formats::default(),
		}
	}

	pub fn set_template(&mut self, format: &str) -> &mut Self {
		self.template = format.into();
		self
	}

	pub fn with_template(mut self, format: &str) -> Self {
		self.set_template(format);
		self
	}
}

impl RecordFormatter for Line {
	fn format(&self, record: &Record) -> String {
		let replacer = RecordReplacer::new(&record, &self.formats);
		PLACEHOLDER_RE.replace_all(&self.template, replacer).to_string()
	}
}

impl date::Formatting for Line {
	fn set_date_format(&mut self, date_format: date::Format) -> &mut Self {
		self.formats.set_date_format(date_format);
		self
	}
}

impl level::Formatting for Line {
	fn set_level_format(&mut self, level_format: level::Format) -> &mut Self {
		self.formats.set_level_format(level_format);
		self
	}
}

impl context::Formatting for Line {
	fn set_context_formatter(
		&mut self,
		context_renderer: Box<context::Formatter + Send + Sync + 'static>,
	) -> &mut Self {
		self.formats.set_context_formatter(context_renderer);
		self
	}
}
