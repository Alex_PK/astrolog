/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use regex::{Captures, Regex};
use serde_json::{Map, Value};

use crate::formatter::{context, date, level, Formats, RecordFormatter, RecordReplacer, PLACEHOLDER_RE};
use crate::logger::{Context, Record};

#[derive(Debug)]
pub struct Html {
	template_line: String,
	formats: Formats,
}

impl Html {
	pub const DEFAULT_TEMPLATE: &'static str =
		"<tr><td>{{ datetime }}</td<td>{{ level }}</td><td>{{ message }}</td><td>{{ context }}</td></tr>\n";

	pub fn new() -> Html {
		Html {
			template_line: Html::DEFAULT_TEMPLATE.into(),
			formats: Formats::default(),
		}
	}

	pub fn set_template(&mut self, template: &str) -> &mut Self {
		self.template_line = template.into();
		self
	}

	pub fn with_template(mut self, template: &str) -> Self {
		self.set_template(template);
		self
	}
}

impl RecordFormatter for Html {
	fn format(&self, record: &Record) -> String {
		let escaped_record = html_special_chars_record(&record);
		let mut res = String::new();
		let replacer = RecordReplacer::new(&escaped_record, &self.formats);
		res.push_str(
			&PLACEHOLDER_RE
				.replace_all(&self.template_line, replacer.clone())
				.to_string(),
		);

		res
	}
}

impl date::Formatting for Html {
	fn set_date_format(&mut self, date_format: date::Format) -> &mut Self {
		self.formats.set_date_format(date_format);
		self
	}
}

impl level::Formatting for Html {
	fn set_level_format(&mut self, level_format: level::Format) -> &mut Self {
		self.formats.set_level_format(level_format);
		self
	}
}

impl context::Formatting for Html {
	fn set_context_formatter(
		&mut self,
		context_renderer: Box<context::Formatter + Send + Sync + 'static>,
	) -> &mut Self {
		self.formats.set_context_formatter(context_renderer);
		self
	}
}

fn html_special_chars_record(record: &Record) -> Record {
	Record {
		dt: record.dt.clone(),
		level: record.level.clone(),
		msg: html_special_chars(&record.msg),
		context: Context::from_json(html_special_chars_value(record.context.as_value())),
	}
}

fn html_special_chars_value(v: &Value) -> Value {
	match v {
		&Value::Object(ref o) => Value::Object(
			o.iter()
				.map(|(k, v)| (k.clone(), html_special_chars_value(v)))
				.collect::<Map<String, Value>>(),
		),
		&Value::Array(ref a) => Value::Array(a.iter().map(|v| html_special_chars_value(v)).collect::<Vec<Value>>()),
		&Value::String(ref s) => Value::String(html_special_chars(s)),
		v => v.clone(),
	}
}

fn html_special_chars(s: &str) -> String {
	lazy_static! {
		static ref ENT_RE: Regex = Regex::new("[<>&]").unwrap();
	}

	let res = ENT_RE.replace_all(s, |caps: &Captures| {
		match &caps[0] {
			"&" => "&amp;",
			"<" => "&lt;",
			">" => "&gt;",
			c => c,
		}
		.to_string()
	});

	res.to_string()
}

#[cfg(test)]
mod tests {
	use super::super::super::Level;
	use super::*;
	use serde_json::json;

	#[test]
	fn basic() {
		let record = Record::new(Level::Debug, "Test").with("key", "value");

		let fmt = Html::new();
		let res = fmt.format(&record);

		assert_eq!("<tr><td>", &res[0..8]);
		assert_eq!(
			"</td<td>DEBG</td><td>Test</td><td>{key: \"value\"}</td></tr>\n",
			&res[27..]
		);
	}

	#[test]
	fn string_html_special_chars() {
		assert_eq!("Normal text", html_special_chars("Normal text"));
		assert_eq!(
			"&lt;title&gt;Text&lt;/title&gt;",
			html_special_chars("<title>Text</title>")
		);
		assert_eq!("Text &amp; numbers", html_special_chars("Text & numbers"));
		assert_eq!(
			"&lt;title&gt;Text &amp; numbers&lt;/title&gt;",
			html_special_chars("<title>Text & numbers</title>")
		);
	}

	#[test]
	fn value_html_special_chars() {
		let v = json!({
			"o": {
				"v1": "<title>A string value</title>",
				"v2": 123,
				"v3": 321.54,
				"v4": true,
				"v5": ["<p>A paragraph & a number</p>", 555]
			},
			"a": [
				"A row<br>Another row",
				false
			]
		});

		assert_eq!(
			r##"{"a":["A row&lt;br&gt;Another row",false],"o":{"v1":"&lt;title&gt;A string value&lt;/title&gt;","v2":123,"v3":321.54,"v4":true,"v5":["&lt;p&gt;A paragraph &amp; a number&lt;/p&gt;",555]}}"##,
			html_special_chars_value(&v).to_string()
		)
	}

	#[test]
	fn record_html_special_chars() {
		let r = Record::new(Level::Debug, "<h1>A big text</h1>")
			.with("v1", "<title>A string value</title>")
			.with("v2", 123)
			.with("v3", 321.54)
			.with("v4", true)
			.with("v5", json!(["<p>A paragraph & a number</p>", 555]));

		let converted = html_special_chars_record(&r);

		assert_eq!("&lt;h1&gt;A big text&lt;/h1&gt;", converted.msg);
		assert_eq!(
			r##"{"v1":"&lt;title&gt;A string value&lt;/title&gt;","v2":123,"v3":321.54,"v4":true,"v5":["&lt;p&gt;A paragraph &amp; a number&lt;/p&gt;",555]}"##,
			&converted.context.as_value().to_string()
		);
	}

}
