/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use serde_json::Value;

use super::Context;

#[derive(Debug)]
pub struct Json5 {}

impl Json5 {
	fn json5(v: &Value) -> String {
		match v {
			&Value::String(ref s) => format!("\"{}\"", s),
			&Value::Number(ref n) => n.to_string(),
			&Value::Bool(ref b) => (if *b { "true" } else { "false" }).into(),
			&Value::Null => "null".into(),
			&Value::Array(ref a) => format!(
				"[{}]",
				a.iter().map(|v| Json5::json5(v)).collect::<Vec<String>>().join(", ")
			),
			&Value::Object(ref a) => format!(
				"{{{}}}",
				a.iter()
					.map(|(k, v)| format!("{}: {}", k, Json5::json5(v)))
					.collect::<Vec<String>>()
					.join(", ")
			),
		}
	}
}

impl super::Formatter for Json5 {
	fn render(&self, context: &Context) -> String {
		Json5::json5(context.as_value())
	}
}
