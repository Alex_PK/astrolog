/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::fmt;

use crate::logger::Context;

mod json;
mod json5;

pub use self::json::Json;
pub use self::json5::Json5;

pub trait Formatter: fmt::Debug + Send + Sync {
	fn render(&self, context: &Context) -> String;
}

pub trait Formatting {
	fn set_context_formatter(&mut self, context_renderer: Box<Formatter + Send + Sync + 'static>) -> &mut Self;
}
