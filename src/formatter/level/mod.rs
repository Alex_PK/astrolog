/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::fmt;

use crate::logger::Level;

pub enum Format {
	None,
	LowerShort,
	Short,
	UpperShort,
	LowerLong,
	Long,
	UpperLong,
	Custom(Box<Fn(&Level) -> String + Send + Sync + 'static>),
}

impl Format {
	pub fn format(&self, level: &Level) -> String {
		match self {
			&Format::None => "".into(),
			&Format::Short => level.short(),
			&Format::Long => level.long(),
			&Format::LowerShort => level.short().to_lowercase(),
			&Format::LowerLong => level.long().to_lowercase(),
			&Format::UpperShort => level.short().to_uppercase(),
			&Format::UpperLong => level.long().to_uppercase(),
			&Format::Custom(ref f) => f(level),
		}
	}
}

impl fmt::Debug for Format {
	fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		match self {
			&Format::Custom(_) => write!(f, "Custom function"),
			a => write!(f, "{:?}", a),
		}
	}
}

pub trait Formatting {
	fn set_level_format(&mut self, _format: Format) -> &mut Self
	where
		Self: Sized,
	{
		self
	}

	fn with_level_format(mut self, format: Format) -> Self
	where
		Self: Sized,
	{
		self.set_level_format(format);
		self
	}
}
