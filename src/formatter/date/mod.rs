/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use chrono::{DateTime, Utc};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Format {
	None,
	IsoShort,
	IsoLong,
	Rfc3339,
	Email,
	Rfc2822,
	Common,
	Intl,
	Custom(String),
}

impl Format {
	pub fn format(&self, dt: &DateTime<Utc>) -> String {
		match self {
			&Format::None => "".to_string(),
			&Format::IsoShort => format!("{}", dt.format("%Y-%m-%dT%H:%M:%S")),
			&Format::IsoLong | &Format::Rfc3339 => format!("{}", dt.to_rfc3339()),
			&Format::Email | &Format::Rfc2822 => format!("{}", dt.to_rfc2822()),
			&Format::Common => format!("{}", dt.format("%d/%b/%Y:%H:%M:%S %z")),
			&Format::Intl => format!("{}", dt.format("%Y-%m-%d %H:%M:%S")),
			&Format::Custom(ref f) => format!("{}", dt.format(&f)),
		}
	}
}

impl<S: ToString> From<S> for Format {
	fn from(s: S) -> Self {
		Format::Custom(s.to_string())
	}
}

pub trait Formatting {
	fn set_date_format(&mut self, _format: Format) -> &mut Self
	where
		Self: Sized,
	{
		self
	}

	fn with_date_format(mut self, format: Format) -> Self
	where
		Self: Sized,
	{
		self.set_date_format(format);
		self
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use chrono::TimeZone;

	#[test]
	fn date_formats() {
		let d = Utc.ymd(2004, 5, 6).and_hms(13, 2, 3);

		assert_eq!("", format!("{}", Format::None.format(&d)));
		assert_eq!("2004-05-06T13:02:03", format!("{}", Format::IsoShort.format(&d)));
		assert_eq!("2004-05-06T13:02:03+00:00", format!("{}", Format::IsoLong.format(&d)));
		assert_eq!(
			"Thu,  6 May 2004 13:02:03 +0000",
			format!("{}", Format::Email.format(&d))
		);
		assert_eq!("06/May/2004:13:02:03 +0000", format!("{}", Format::Common.format(&d)));
		assert_eq!("2004-05-06 13:02:03", format!("{}", Format::Intl.format(&d)));
	}
}
