/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::fmt;

use chrono::{DateTime, Utc};
use regex::{Captures, Regex, Replacer};
use serde_json::Value;

use crate::logger::{Level, Record};
use crate::Context;

pub mod context;
pub mod date;
pub mod level;
pub mod record;
pub mod fields;

pub use self::context::{Formatter as ContextFormatter, Formatting as ContextFormatting};
pub use self::date::{Format as DateFormat, Formatting as DateFormatting};
pub use self::level::{Format as LevelFormat, Formatting as LevelFormatting};
pub use self::fields::{Names as FieldNames, Naming as FieldNaming};

pub use self::record::html::Html as HtmlFormatter;
pub use self::record::json::Json as JsonFormatter;
pub use self::record::line::Line as LineFormatter;


lazy_static! {
	pub static ref RE: Regex = Regex::new(r##"\{\{ *([a-z0-9._/~-]+) *\}\}"##).unwrap();
	static ref MESSAGE_RE: Regex = Regex::new(r##"\{\{[^a-z]*message[^a-z]*\}\}"##).unwrap();

	// Placeholders:
	// + will be changed to a single space
	// ? in the suffix means if the value is empty, the whole placeholder will not be shown
	// Examples:
	// {{ placeholder }}
	// {{ +placeholder+ }} -> add a space before and after
	// {{ +placeholder+? }} -> only print the text and the spaces before and after IF the text is not empty
	// {{ +[placeholder]? }} -> print the text enclosed in [] and prefixed by a space IF it's not empty
	pub static ref PLACEHOLDER_RE: Regex = Regex::new(r##"\{\{\s*(?P<prefix>[^ a-z0-9._/~-]*)(?P<placeholder>[a-z0-9._/~-]+)(?P<suffix>[^ a-z0-9._/~-]*)\s*\}\}"##).unwrap();
}

#[derive(Debug)]
pub struct Formats {
	pub fields: FieldNames,
	pub date: date::Format,
	pub level: level::Format,
	pub context: Box<context::Formatter>,
}

impl Formats {
	pub fn new() -> Formats {
		Formats {
			fields: FieldNames::default(),
			date: date::Format::Intl,
			level: level::Format::UpperShort,
			context: Box::new(context::Json5 {}),
		}
	}

	pub fn default() -> Formats {
		Formats::new()
	}

	pub fn date(&self, dt: &DateTime<Utc>) -> String {
		self.date.format(dt)
	}

	pub fn level(&self, level: &Level) -> String {
		self.level.format(level)
	}

	pub fn context(&self, ctx: &Context) -> String {
		self.context.render(ctx)
	}
}

impl DateFormatting for Formats {
	fn set_date_format(&mut self, date_format: DateFormat) -> &mut Self {
		self.date = date_format;
		self
	}
}

impl LevelFormatting for Formats {
	fn set_level_format(&mut self, level_format: LevelFormat) -> &mut Self {
		self.level = level_format;
		self
	}
}

impl ContextFormatting for Formats {
	fn set_context_formatter(&mut self, context_formatter: Box<ContextFormatter + Send + Sync + 'static>) -> &mut Self {
		self.context = context_formatter;
		self
	}
}

impl FieldNaming for Formats {
	fn set_field_names(&mut self, names: FieldNames) -> &mut Self {
		self.fields = names;
		self
	}
}

pub trait RecordFormatter: fmt::Debug + Send + Sync {
	fn format(&self, record: &Record) -> String;
}

#[derive(Clone)]
pub struct RecordReplacer<'a> {
	record: &'a Record,
	formats: &'a Formats,
}

impl<'a> RecordReplacer<'a> {
	pub fn new(record: &'a Record, formats: &'a Formats) -> RecordReplacer<'a> {
		RecordReplacer { record, formats }
	}

	pub fn replace(placeholder: &str, record: &Record, formats: &Formats) -> String {
		let temp = PLACEHOLDER_RE.captures(placeholder);
		if temp.is_none() {
			return "".into();
		}
		let parts = temp.unwrap();
		let prefix = parts.name("prefix").map(|p| p.as_str().to_owned()).unwrap_or_default();
		let suffix = parts.name("suffix").map(|p| p.as_str().to_owned()).unwrap_or_default();
		let placeholder = parts
			.name("placeholder")
			.map(|p| p.as_str().to_owned())
			.unwrap_or_default();
		if placeholder.is_empty() {
			return "".into();
		}

		let formatted = match placeholder.as_ref() {
			"message" => {
				// Avoid infinite loop
				let message = MESSAGE_RE.replace_all(&record.msg, "");
				let message = PLACEHOLDER_RE.replace_all(&message, RecordReplacer::new(record, formats));
				message.into()
			}
			"level" => formats.level(&record.level),
			"datetime" => formats.date(&record.dt),
			"context" => {
				if record.context.is_empty() && suffix.contains('?') {
					"".into()
				} else {
					formats.context(&record.context)
				}
			}
			s => record
				.context
				.pointer(
					&(if s.starts_with("/") {
						s.to_owned()
					} else {
						format!("/{}", s)
					}),
				)
				.map(|s| match s {
					&Value::String(ref s) => s.as_str().to_owned(),
					&Value::Number(ref n) => format!("{}", n.as_f64().unwrap_or(0.0)),
					ref v => v.to_string(),
				})
				.unwrap_or(placeholder.to_string()),
		};

		if suffix.contains('?') && formatted.is_empty() {
			return formatted;
		}

		format!(
			"{}{}{}",
			prefix.replace('+', " "),
			formatted,
			suffix.replace('+', " ").replace('?', "")
		)
	}
}

impl<'a> Replacer for RecordReplacer<'a> {
	fn replace_append(&mut self, caps: &Captures, dst: &mut String) {
		dst.push_str(&RecordReplacer::replace(&caps[0], self.record, self.formats));
	}
}

#[cfg(test)]
mod tests {
	use super::super::Level;
	use super::*;

	#[test]
	fn replacer_basic() {
		let record = Record::new(Level::Debug, "Some message").with("k", "v");
		let formats = Formats::default();
		let res = RE.replace_all(
			"[{{ datetime }}] {{ level }} {{ message }} {{ context }}",
			RecordReplacer::new(&record, &formats),
		);

		assert_eq!("] DEBG Some message {k: \"v\"}", &res[20..]);
	}

	#[test]
	fn replacer_with_unknown() {
		let record = Record::new(Level::Debug, "Some message").with("k", "v");
		let formats = Formats::default();
		let res = RE.replace_all(
			"[{{ datetime }}] {{ level }} {{ unknown }} {{ message }} 15% {{ context }}",
			RecordReplacer::new(&record, &formats),
		);

		assert_eq!("] DEBG unknown Some message 15% {k: \"v\"}", &res[20..]);
	}

	#[test]
	fn replacer_with_unknown_and_context() {
		let record = Record::new(Level::Debug, "Some {{ k }} message").with("k", "v");
		let formats = Formats::default();
		let res = RE.replace_all(
			"[{{ datetime }}] {{ level }} {{ unknown }} {{ message }} 15% {{ context }}",
			RecordReplacer::new(&record, &formats),
		);

		assert_eq!("] DEBG unknown Some v message 15% {k: \"v\"}", &res[20..]);
	}

	#[test]
	fn replacer_with_message_in_message() {
		let record = Record::new(Level::Debug, "Some {{ message+ }}weirdness").with("message", "more");
		let formats = Formats::default();
		let res = RE.replace_all(
			"[{{ datetime }}] {{ level }} {{ unknown }} {{ message }} {{ context }}",
			RecordReplacer::new(&record, &formats),
		);

		assert_eq!("] DEBG unknown Some weirdness {message: \"more\"}", &res[20..]);
	}

	#[test]
	fn replacer_with_prefixed_message_in_message() {
		let record = Record::new(Level::Debug, "Some {{ amessage+ }}weirdness").with("amessage", "more");
		let formats = Formats::default();
		let res = RE.replace_all(
			"[{{ datetime }}] {{ level }} {{ unknown }} {{ message }} {{ context }}",
			RecordReplacer::new(&record, &formats),
		);

		assert_eq!("] DEBG unknown Some more weirdness {amessage: \"more\"}", &res[20..]);
	}

	#[test]
	fn replacer_with_suffixed_message_in_message() {
		let record = Record::new(Level::Debug, "Some {{ messages+ }}weirdness").with("messages", "more");
		let formats = Formats::default();
		let res = RE.replace_all(
			"[{{ datetime }}] {{ level }} {{ unknown }} {{ message }} {{ context }}",
			RecordReplacer::new(&record, &formats),
		);

		assert_eq!("] DEBG unknown Some more weirdness {messages: \"more\"}", &res[20..]);
	}
}
