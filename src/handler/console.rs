/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::collections::HashSet;
use std::io::{self, Write};

use crate::errors::HandlerResult;
use crate::formatter::{LineFormatter, RecordFormatter};
use crate::handler::{Handler, LevelAware};
use crate::logger::{Level, Record};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum ConsoleOutput {
	Stdout,
	Stderr,
}

#[derive(Debug)]
pub struct ConsoleHandler {
	output: ConsoleOutput,
	levels: HashSet<Level>,
	formatter: Box<RecordFormatter>,
}

impl ConsoleHandler {
	pub fn new() -> ConsoleHandler {
		ConsoleHandler {
			output: ConsoleOutput::Stderr,
			levels: Level::all_set(),
			formatter: Box::new(LineFormatter::new()),
		}
	}

	pub fn set_stdout(&mut self) -> &mut Self {
		self.output = ConsoleOutput::Stdout;
		self
	}

	pub fn with_stdout(mut self) -> Self {
		self.set_stdout();
		self
	}

	pub fn set_stderr(&mut self) -> &mut Self {
		self.output = ConsoleOutput::Stderr;
		self
	}

	pub fn with_stderr(mut self) -> Self {
		self.set_stderr();
		self
	}

	pub fn set_formatter<F: RecordFormatter + 'static>(&mut self, formatter: F) -> &mut Self {
		self.formatter = Box::new(formatter);
		self
	}

	pub fn with_formatter<F: RecordFormatter + 'static>(mut self, formatter: F) -> Self {
		self.set_formatter(formatter);
		self
	}
}

impl Handler for ConsoleHandler {
	fn is_handling(&self, level: &Level) -> bool {
		self.levels.contains(level)
	}

	fn handle(&self, record: &Record) -> HandlerResult {
		if self.is_handling(&record.level) {
			let to_log = self.formatter.format(record);
			match self.output {
				ConsoleOutput::Stdout => {
					let out = io::stdout();
					let mut handle = out.lock();
					let _ = handle.write(to_log.as_bytes());
				}
				ConsoleOutput::Stderr => {
					let out = io::stderr();
					let mut handle = out.lock();
					let _ = handle.write(to_log.as_bytes());
				}
			}
		}
		Ok(true)
	}
}

impl LevelAware for ConsoleHandler {
	fn set_levels(&mut self, levels: &[Level]) -> &mut Self {
		self.levels = levels.iter().cloned().collect();
		self
	}
}
