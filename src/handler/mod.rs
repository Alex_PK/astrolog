/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::sync::RwLock;

use crate::errors::HandlerResult;
use crate::logger::{Level, Record};
use crate::handler::console::ConsoleHandler;

pub mod console;
pub mod null;
pub mod vec;

pub trait Handler {
	fn is_handling(&self, _level: &Level) -> bool {
		false
	}

	fn handle(&self, _record: &Record) -> HandlerResult {
		Ok(true)
	}
}

pub trait LevelAware {
	fn set_levels(&mut self, _levels: &[Level]) -> &mut Self;

	fn with_levels(mut self, levels: &[Level]) -> Self
	where
		Self: Sized,
	{
		self.set_levels(levels);
		self
	}

	fn set_levels_range(&mut self, from: Level, to: Level) -> &mut Self {
		self.set_levels(Level::range(from, to));
		self
	}

	fn with_levels_range(mut self, from: Level, to: Level) -> Self
	where
		Self: Sized,
	{
		self.set_levels_range(from, to);
		self
	}
}

pub struct Handlers {
	handlers: RwLock<Vec<Box<Handler + Sync + Send + 'static>>>,
	default: Box<Handler + Sync + Send + 'static>,
}

impl Handlers {
	pub fn new() -> Handlers {
		Handlers {
			handlers: RwLock::new(Vec::new()),
			default: Box::new(ConsoleHandler::new()),
		}
	}
	pub fn push<T>(&self, handler: T)
		where
			T: Handler + Sync + Send + 'static,
	{
		match self.handlers.write() {
			Ok(mut handlers) => handlers.push(Box::new(handler)),
			Err(e) => eprintln!("Error adding the handler: {:?}", e),
		}
	}

	pub fn len(&self) -> usize {
		self.handlers.read().map(|h| h.len()).unwrap_or(0)
	}

	pub fn is_empty(&self) -> bool {
		self.len() == 0
	}

	pub fn handle(&self, record: &Record, print_errors: bool) {
		match self.handlers.read() {
			Ok(handlers) => {
				if handlers.is_empty() {
					let _ = self.default.handle(record);
				} else {
					handlers
						.iter()
						.filter(|h| h.is_handling(&record.level))
						.all(|h| match h.handle(record) {
							Ok(cont) => cont,
							Err(e) => {
								if print_errors {
									eprintln!("{:?}", e);
								}
								true
							}
						});
				}
			}
			Err(e) => eprintln!("Error locking handlers: {:?}", e),
		}
	}
}
