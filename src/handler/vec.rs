/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::collections::HashSet;
use std::sync::{Arc, RwLock};

use crate::errors::HandlerResult;
use crate::handler::{Handler, LevelAware};
use crate::logger::{Level, Record};

#[derive(Debug, Clone)]
pub struct VecHandlerStore(Arc<RwLock<Vec<Record>>>);

impl VecHandlerStore {
	pub fn new() -> VecHandlerStore {
		VecHandlerStore(Arc::new(RwLock::new(Vec::new())))
	}

	pub fn push(&self, record: Record) {
		let _ = self.0.write().map(|mut r| r.push(record.clone()));
	}

	pub fn get_all(&self) -> Vec<Record> {
		self.0.read().map(|r| r.clone()).unwrap_or_default()
	}

	pub fn take(&self) -> Vec<Record> {
		self.0.write().map(|mut lock| lock.drain(..).collect()).unwrap_or_default()
	}
}

#[derive(Debug)]
pub struct VecHandler {
	store: VecHandlerStore,
	levels: HashSet<Level>,
}

impl VecHandler {
	pub fn new() -> VecHandler {
		VecHandler {
			store: VecHandlerStore::new(),
			levels: Level::all_set(),
		}
	}

	pub fn get_store(&self) -> VecHandlerStore {
		self.store.clone()
	}
}

impl Handler for VecHandler {
	fn is_handling(&self, level: &Level) -> bool {
		self.levels.contains(level)
	}

	fn handle(&self, record: &Record) -> HandlerResult {
		if self.is_handling(&record.level) {
			self.store.push(record.clone());
		}
		Ok(true)
	}
}

impl LevelAware for VecHandler {
	fn set_levels(&mut self, levels: &[Level]) -> &mut Self {
		self.levels = levels.iter().cloned().collect();
		self
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn handler_with_levels() {
		let _ = VecHandler::new().with_levels(Level::all());
	}
}
