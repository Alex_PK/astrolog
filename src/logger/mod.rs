/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use serde::Serialize;
use std::collections::HashSet;
use std::error::Error;
use std::fmt;
use std::rc::Rc;
use std::sync::{mpsc, Arc};
use std::thread;

use crate::handler::{Handler, LevelAware, Handlers};

mod context;
mod item;
mod level;
mod record;

pub use self::context::Context;
pub use self::item::Item;
pub use self::level::Level;
pub use self::record::Record;

//type Handlers = Vec<Box<Handler + Sync + Send + 'static>>;

pub struct Logger {
	handlers: Arc<Handlers>,
	context: Context,
	levels: HashSet<Level>,
	q: Option<mpsc::SyncSender<Record>>,
	worker: Option<thread::JoinHandle<()>>,
	print_errors: bool,
}

impl Logger {
	pub fn new() -> Logger {
		Logger {
			handlers: Arc::new(Handlers::new()),
			context: Context::new(),
			levels: Level::all_set(),
			q: None,
			worker: None,
			print_errors: false,
		}
	}

	pub fn into_rc(self) -> Rc<Logger> {
		Rc::new(self)
	}

	pub fn into_arc(self) -> Arc<Logger> {
		Arc::new(self)
	}

	pub fn set_print_errors(&mut self, print_errors: bool) -> &mut Self {
		self.print_errors = print_errors;
		self
	}

	pub fn print_errors(mut self, print_errors: bool) -> Self {
		self.set_print_errors(print_errors);
		self
	}

	pub fn push_handler<T>(&mut self, handler: T) -> &mut Self
	where
		T: Handler + Sync + Send + 'static,
	{
		self.handlers.push(handler);
		self
	}

	pub fn with_handler<T>(mut self, handler: T) -> Self
	where
		T: Handler + Sync + Send + 'static,
	{
		self.push_handler(handler);
		self
	}

	pub fn set_global<V>(&mut self, k: &str, v: V) -> &mut Self
	where
		V: Serialize,
	{
		self.context.set(k, v);
		self
	}

	pub fn with_global<V>(mut self, k: &str, v: V) -> Self
	where
		V: Serialize,
	{
		self.set_global(k, v);
		self
	}

	pub fn make_sync(&mut self) -> &mut Self {
		if let Some(q) = self.q.take() {
			drop(q);
			if let Some(worker) = self.worker.take() {
				worker.join().expect("Unable to join logger worker");
			}
		}
		self
	}

	pub fn sync(mut self) -> Self {
		self.make_sync();
		self
	}

	pub fn make_async(&mut self) -> &mut Self {
		if let Some(_) = self.q {
			return self;
		}

		let handlers = self.handlers.clone();
		let (sender, receiver) = mpsc::sync_channel(256);
		let print_errors = self.print_errors;
		self.q = Some(sender);

		let worker = thread::spawn(move || {
			for record in receiver {
				handlers.handle(&record, print_errors);
			}
		});
		self.worker = Some(worker);

		self
	}

	pub fn r#async(mut self) -> Self {
		self.make_async();
		self
	}

	pub fn with<V>(&self, k: &str, v: V) -> Item
	where
		V: Serialize,
	{
		Item::new(self).with(k, v)
	}

	pub fn with_multi<V: Into<Context>>(&self, ctx: V) -> Item {
		Item::new(self).with_multi(ctx.into())
	}

	pub fn with_error(&self, e: &Error) -> Item {
		Item::new(self).with_error(e)
	}

	pub fn with_new_context(&self, ctx: Context) -> Item {
		Item::new(self).with_new_context(ctx)
	}

	pub fn log<S: ToString>(&self, level: Level, msg: S) {
		if !self.levels.contains(&level) {
			return;
		}
		let record = Record::new(level, msg.to_string());
		self.dispatch(record);
	}

	pub fn trace<S: ToString>(&self, msg: S) {
		self.log(Level::Trace, msg);
	}
	pub fn profile<S: ToString>(&self, msg: S) {
		self.log(Level::Profile, msg);
	}
	pub fn debug<S: ToString>(&self, msg: S) {
		self.log(Level::Debug, msg);
	}
	pub fn info<S: ToString>(&self, msg: S) {
		self.log(Level::Info, msg);
	}
	pub fn notice<S: ToString>(&self, msg: S) {
		self.log(Level::Notice, msg);
	}
	pub fn warning<S: ToString>(&self, msg: S) {
		self.log(Level::Warning, msg);
	}
	pub fn error<S: ToString>(&self, msg: S) {
		self.log(Level::Error, msg);
	}
	pub fn critical<S: ToString>(&self, msg: S) {
		self.log(Level::Critical, msg);
	}
	pub fn alert<S: ToString>(&self, msg: S) {
		self.log(Level::Alert, msg);
	}
	pub fn emergency<S: ToString>(&self, msg: S) {
		self.log(Level::Emergency, msg);
	}

	fn dispatch(&self, mut record: Record) {
		record.merge_context(&self.context);

		if let Some(ref q) = self.q {
			// We don't want to block the app if the channel is full. Ignore the error.
			let _ = q.try_send(record);
		} else {
			self.handlers.handle(&record, self.print_errors);
		}
	}
}

impl LevelAware for Logger {
	fn set_levels(&mut self, new_levels: &[Level]) -> &mut Self {
		self.levels = new_levels.iter().cloned().collect();
		self
	}
}

impl fmt::Debug for Logger {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(
			f,
			"Logger {{ Handlers: {} Context: {:?} }}",
			self.handlers.len(),
			self.context
		)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::handler::{null::NullHandler, vec::VecHandler, LevelAware};

	#[test]
	fn test_build_with_no_handler() {
		let logger = Logger::new();

		logger.debug("a debug message");
		logger.log(Level::Emergency, "an emergency message");
	}

	#[test]
	fn test_build_with_null_handler() {
		let logger = Logger::new().with_handler(NullHandler {});

		logger.debug("a debug message");
		logger.log(Level::Emergency, "an emergency message");
	}

	#[test]
	fn test_build_with_async_dispatcher() {
		let logger = Logger::new().r#async().with_handler(NullHandler {});

		logger.debug("a debug message");
		logger.log(Level::Emergency, "an emergency message");
	}

	#[test]
	fn test_context() {
		let logger = Logger::new()
			.with_global("string", "some string")
			.with_global("bool", true)
			.with_global("array", vec!["a", "b"]);

		let context = &logger.context;
		assert!(context.contains_key("string"));
		assert_eq!("some string", context.get("string").unwrap());

		assert!(context.contains_key("bool"));
		assert_eq!(true, context.get("bool").unwrap().as_bool().unwrap());

		assert!(context.contains_key("array"));
		assert_eq!("a", context.get("array").unwrap().as_array().unwrap()[0]);
		assert_eq!("b", context.get("array").unwrap().as_array().unwrap()[1]);
	}

	#[test]
	fn test_levels() {
		let handler = VecHandler::new();
		let store = handler.get_store();
		let logger = Logger::new().with_handler(handler);

		logger.trace("Trace message");
		logger.profile("Profile message");
		logger.debug("Debug message");
		logger.info("Info message");
		logger.notice("Notice message");
		logger.warning("Warning message");
		logger.error("Error message");
		logger.critical("Critical message");
		logger.alert("Alert message");
		logger.emergency("Emergency message");

		let logs = store.take();

		assert_eq!(10, logs.len());

		assert_eq!("Trace message", logs[0].msg);
		assert_eq!(Level::Trace, logs[0].level);

		assert_eq!("Profile message", logs[1].msg);
		assert_eq!(Level::Profile, logs[1].level);

		assert_eq!("Debug message", logs[2].msg);
		assert_eq!(Level::Debug, logs[2].level);

		assert_eq!("Info message", logs[3].msg);
		assert_eq!(Level::Info, logs[3].level);

		assert_eq!("Notice message", logs[4].msg);
		assert_eq!(Level::Notice, logs[4].level);

		assert_eq!("Warning message", logs[5].msg);
		assert_eq!(Level::Warning, logs[5].level);

		assert_eq!("Error message", logs[6].msg);
		assert_eq!(Level::Error, logs[6].level);

		assert_eq!("Critical message", logs[7].msg);
		assert_eq!(Level::Critical, logs[7].level);

		assert_eq!("Alert message", logs[8].msg);
		assert_eq!(Level::Alert, logs[8].level);

		assert_eq!("Emergency message", logs[9].msg);
		assert_eq!(Level::Emergency, logs[9].level);
	}

	#[test]
	fn test_with_levels() {
		let handler = VecHandler::new();
		let store = handler.get_store();
		let logger = Logger::new()
			.with_levels(&[Level::Warning, Level::Emergency])
			.with_handler(handler);

		logger.trace("Trace message");
		logger.profile("Profile message");
		logger.debug("Debug message");
		logger.info("Info message");
		logger.notice("Notice message");
		logger.warning("Warning message");
		logger.error("Error message");
		logger.critical("Critical message");
		logger.alert("Alert message");
		logger.emergency("Emergency message");

		let logs = store.take();

		assert_eq!(2, logs.len());

		assert_eq!("Warning message", logs[0].msg);
		assert_eq!(Level::Warning, logs[0].level);

		assert_eq!("Emergency message", logs[1].msg);
		assert_eq!(Level::Emergency, logs[1].level);
	}

	#[test]
	fn test_with_handler_levels() {
		let handler = VecHandler::new().with_levels(&[Level::Warning, Level::Emergency]);

		let store = handler.get_store();
		let logger = Logger::new().with_handler(handler);

		logger.trace("Trace message");
		logger.profile("Profile message");
		logger.debug("Debug message");
		logger.info("Info message");
		logger.notice("Notice message");
		logger.warning("Warning message");
		logger.error("Error message");
		logger.critical("Critical message");
		logger.alert("Alert message");
		logger.emergency("Emergency message");

		let logs = store.take();

		assert_eq!(2, logs.len());

		assert_eq!("Warning message", logs[0].msg);
		assert_eq!(Level::Warning, logs[0].level);

		assert_eq!("Emergency message", logs[1].msg);
		assert_eq!(Level::Emergency, logs[1].level);
	}

	#[test]
	fn multi_thread_logger() {
		use std::thread;

		let handler = VecHandler::new();
		let store = handler.get_store();
		let logger = Logger::new().with_handler(handler).into_arc();
		let tlogger = logger.clone();

		let th = thread::spawn(move || {
			tlogger.debug("Thread debug message");
		});

		logger.warning("Main warning message");

		let _ = th.join();

		let logs = store.take();

		assert!(logs.iter().position(|v| v.msg.contains("Thread debug")).is_some());
		assert!(logs.iter().position(|v| v.msg.contains("Main warning")).is_some());
	}

	#[test]
	fn test_levels_with_item() {
		let handler = VecHandler::new();
		let store = handler.get_store();
		let logger = Logger::new().with_handler(handler);

		logger.with("kt", "vt").trace("Trace message");
		logger.with("kp", "vp").profile("Profile message");
		logger.with("kd", "vd").debug("Debug message");
		logger.with("ki", "vi").info("Info message");
		logger.with("kn", "vn").notice("Notice message");
		logger.with("kw", "vw").warning("Warning message");
		logger.with("ke", "ve").error("Error message");
		logger.with("kc", "vc").critical("Critical message");
		logger.with("ka", "va").alert("Alert message");
		logger.with("km", "vm").emergency("Emergency message");

		let logs = store.take();

		assert_eq!(10, logs.len());

		assert_eq!("Trace message", logs[0].msg);
		assert_eq!(Level::Trace, logs[0].level);
		assert_eq!("vt", logs[0].context.get("kt").expect("Missing expected context vt"));

		assert_eq!("Profile message", logs[1].msg);
		assert_eq!(Level::Profile, logs[1].level);
		assert_eq!("vp", logs[1].context.get("kp").expect("Missing expected context vp"));

		assert_eq!("Debug message", logs[2].msg);
		assert_eq!(Level::Debug, logs[2].level);
		assert_eq!("vd", logs[2].context.get("kd").expect("Missing expected context vd"));

		assert_eq!("Info message", logs[3].msg);
		assert_eq!(Level::Info, logs[3].level);
		assert_eq!("vi", logs[3].context.get("ki").expect("Missing expected context vi"));

		assert_eq!("Notice message", logs[4].msg);
		assert_eq!(Level::Notice, logs[4].level);
		assert_eq!("vn", logs[4].context.get("kn").expect("Missing expected context vn"));

		assert_eq!("Warning message", logs[5].msg);
		assert_eq!(Level::Warning, logs[5].level);
		assert_eq!("vw", logs[5].context.get("kw").expect("Missing expected context vw"));

		assert_eq!("Error message", logs[6].msg);
		assert_eq!(Level::Error, logs[6].level);
		assert_eq!("ve", logs[6].context.get("ke").expect("Missing expected context ve"));

		assert_eq!("Critical message", logs[7].msg);
		assert_eq!(Level::Critical, logs[7].level);
		assert_eq!("vc", logs[7].context.get("kc").expect("Missing expected context vc"));

		assert_eq!("Alert message", logs[8].msg);
		assert_eq!(Level::Alert, logs[8].level);
		assert_eq!("va", logs[8].context.get("ka").expect("Missing expected context va"));

		assert_eq!("Emergency message", logs[9].msg);
		assert_eq!(Level::Emergency, logs[9].level);
		assert_eq!("vm", logs[9].context.get("km").expect("Missing expected context vm"));
	}

	#[test]
	fn multi_thread_logger_with_item() {
		use std::thread;

		let handler = VecHandler::new();
		let store = handler.get_store();
		let logger = Logger::new().with_handler(handler).into_arc();
		let tlogger = logger.clone();

		let th = thread::spawn(move || {
			tlogger.with("kt", "vt").debug("Thread debug message");
		});

		logger.with("km", "vm").warning("Main warning message");

		let _ = th.join();

		let logs = store.take();

		assert!(logs
			.iter()
			.position(|v| v.msg.contains("Thread debug") && v.context.get("kt").expect("Missing thread context") == "vt")
			.is_some());
		assert!(logs
			.iter()
			.position(|v| v.msg.contains("Main warning") && v.context.get("km").expect("Missing main context") == "vm")
			.is_some());
	}

	#[test]
	fn multi_with_error() {
		use std::io::{Error as IoError, ErrorKind as IoErrorKind};

		let handler = VecHandler::new();
		let store = handler.get_store();
		let logger = Logger::new().with_handler(handler);

		// os_err 1 = permission denied
		let err = IoError::new(IoErrorKind::PermissionDenied, IoError::from_raw_os_error(1));

		logger.with_error(&err).trace("Trace message");

		let logs = store.take();

		assert!(logs
			.iter()
			.position(|v| v.msg.contains("Trace message")
				&& v.context.get("error").expect("Missing error in context") == "permission denied")
			.is_some());
	}
}
