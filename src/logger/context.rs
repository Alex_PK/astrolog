/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::ops::{Deref, DerefMut};

use serde::Serialize;
use serde_json::{to_value, Map, Value, json};

#[derive(Clone, Debug, PartialEq)]
pub struct Context(Value);

impl Deref for Context {
	type Target = Map<String, Value>;

	fn deref(&self) -> &Self::Target {
		match self.0 {
			Value::Object(ref m) => m,
			_ => unreachable!(),
		}
	}
}

impl DerefMut for Context {
	fn deref_mut(&mut self) -> &mut Self::Target {
		match self.0 {
			Value::Object(ref mut m) => m,
			_ => unreachable!(),
		}
	}
}

impl Context {
	pub fn new() -> Self {
		Context(Value::from(Map::new()))
	}

	pub fn from_json(v: Value) -> Self {
		match v {
			Value::Object(_) => Context(v),
			_ => Context(json!({"astrolog_error": "Invalid context"}))
		}
	}

	pub fn set<V>(&mut self, k: &str, v: V) -> &mut Self
	where
		V: Serialize,
	{
		let _ = to_value(v).map(|v| self.insert(k.into(), v));
		self
	}

	pub fn with<V>(mut self, k: &str, v: V) -> Self
	where
		V: Serialize,
	{
		self.set(k, v);
		self
	}

	pub fn pointer(&self, pointer: &str) -> Option<&Value> {
		self.0.pointer(pointer)
	}

	pub fn to_value(&self) -> Value {
		self.0.clone()
	}

	pub fn as_value(&self) -> &Value {
		&self.0
	}
}

impl From<Value> for Context {
	fn from(v: Value) -> Self {
		Context::from_json(v)
	}
}

impl From<&Value> for Context {
	fn from(v: &Value) -> Self {
		Context::from_json(v.to_owned())
	}
}

impl From<Map<String,Value>> for Context {
	fn from(v: Map<String, Value>) -> Self {
		Context::from_json(Value::Object(v))
	}
}

impl From<&Map<String,Value>> for Context {
	fn from(v: &Map<String, Value>) -> Self {
		Context::from_json(Value::Object(v.to_owned()))
	}
}

impl From<&Context> for Context {
	fn from(v: &Context) -> Self {
		v.to_owned()
	}
}
