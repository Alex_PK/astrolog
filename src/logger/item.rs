/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::error::Error;

use serde::Serialize;

use crate::logger::Context;
use crate::ASTROLOG_GLOBAL;
use crate::{Level, Logger, Record};

#[derive(Debug, Clone)]
pub struct Item<'l> {
	logger: Option<&'l Logger>,
	record: Record,
}

impl<'l> Item<'l> {
	pub fn new_global() -> Item<'l> {
		let record = Record::empty();
		Item { logger: None, record }
	}

	pub fn new(logger: &'l Logger) -> Item<'l> {
		let record = Record::empty();
		Item {
			logger: Some(logger),
			record,
		}
	}

	pub fn with<V>(mut self, k: &str, v: V) -> Self
	where
		V: Serialize,
	{
		self.record.set(k, v);
		self
	}

	pub fn with_multi<V: Into<Context>>(mut self, ctx: V) -> Self {
		self.record.override_context(ctx);
		self
	}

	pub fn with_new_context<V: Into<Context>>(mut self, ctx: V) -> Self {
		self.record.set_context(ctx);
		self
	}

	pub fn with_error(mut self, e: &Error) -> Self {
		self.record.set_error(e);
		self
	}

	pub fn log<S: ToString>(mut self, level: Level, msg: S) {
		self.record.level = level;
		self.record.msg = msg.to_string();
		if let Some(logger) = self.logger {
			logger.dispatch(self.record);
		} else {
			ASTROLOG_GLOBAL.read().unwrap().dispatch(self.record);
		}
	}

	pub fn trace<S: ToString>(self, msg: S) {
		self.log(Level::Trace, msg);
	}
	pub fn profile<S: ToString>(self, msg: S) {
		self.log(Level::Profile, msg);
	}
	pub fn debug<S: ToString>(self, msg: S) {
		self.log(Level::Debug, msg);
	}
	pub fn info<S: ToString>(self, msg: S) {
		self.log(Level::Info, msg);
	}
	pub fn notice<S: ToString>(self, msg: S) {
		self.log(Level::Notice, msg);
	}
	pub fn warning<S: ToString>(self, msg: S) {
		self.log(Level::Warning, msg);
	}
	pub fn error<S: ToString>(self, msg: S) {
		self.log(Level::Error, msg);
	}
	pub fn critical<S: ToString>(self, msg: S) {
		self.log(Level::Critical, msg);
	}
	pub fn alert<S: ToString>(self, msg: S) {
		self.log(Level::Alert, msg);
	}
	pub fn emergency<S: ToString>(self, msg: S) {
		self.log(Level::Emergency, msg);
	}
}
