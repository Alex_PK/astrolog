/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::collections::HashSet;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Level {
	None = 0,
	Trace = 1 << 0,
	Profile = 1 << 1,
	Debug = 1 << 2,
	Info = 1 << 3,
	Notice = 1 << 4,
	Warning = 1 << 5,
	Error = 1 << 6,
	Critical = 1 << 7,
	Alert = 1 << 8,
	Emergency = 1 << 9,
}

static ALL_LEVELS: [Level; 10] = [
	Level::Trace,
	Level::Profile,
	Level::Debug,
	Level::Info,
	Level::Notice,
	Level::Warning,
	Level::Error,
	Level::Critical,
	Level::Alert,
	Level::Emergency,
];

impl Level {
	pub fn long(&self) -> String {
		match self {
			&Level::Trace => "Trace",
			&Level::Profile => "Profile",
			&Level::Debug => "Debug",
			&Level::Info => "Info",
			&Level::Notice => "Notice",
			&Level::Warning => "Warning",
			&Level::Error => "Error",
			&Level::Critical => "Critical",
			&Level::Alert => "Alert",
			&Level::Emergency => "Emergency",
			_ => "",
		}
		.to_string()
	}

	pub fn short(&self) -> String {
		match self {
			&Level::Trace => "Trce",
			&Level::Profile => "Prof",
			&Level::Debug => "Debg",
			&Level::Info => "Info",
			&Level::Notice => "Notc",
			&Level::Warning => "Warn",
			&Level::Error => "Erro",
			&Level::Critical => "Crit",
			&Level::Alert => "Alrt",
			&Level::Emergency => "Emrg",
			_ => "",
		}
		.to_string()
	}

	pub fn all() -> &'static [Level] {
		&ALL_LEVELS
	}

	pub fn all_set() -> HashSet<Level> {
		ALL_LEVELS.iter().cloned().collect()
	}

	pub fn range(from: Level, to: Level) -> &'static [Level] {
		let start = ALL_LEVELS.iter().position(|x| x == &from).unwrap_or(0);
		let end = 1 + ALL_LEVELS.iter().rposition(|x| x == &to).unwrap_or(ALL_LEVELS.len());

		&ALL_LEVELS[start..end]
	}

	pub fn range_set(from: Level, to: Level) -> HashSet<Level> {
		Level::range(from, to).iter().cloned().collect()
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn level_print() {
		assert_eq!("Warning".to_string(), Level::Warning.long());
		assert_eq!("Warn".to_string(), Level::Warning.short());
		assert_eq!("Info".to_string(), Level::Info.long());
	}

	#[test]
	fn range() {
		let levels = Level::range(Level::Info, Level::Error);
		assert_eq!([Level::Info, Level::Notice, Level::Warning, Level::Error], levels);

		let levels = Level::range(Level::Trace, Level::Debug);
		assert_eq!([Level::Trace, Level::Profile, Level::Debug], levels);

		let levels = Level::range(Level::Error, Level::Emergency);
		assert_eq!([Level::Error, Level::Critical, Level::Alert, Level::Emergency], levels);
	}
}
