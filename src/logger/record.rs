/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::error::Error;

use chrono::{DateTime, Utc};
use serde::Serialize;

use super::{Context, Level};

#[derive(Debug, Clone)]
pub struct Record {
	pub dt: DateTime<Utc>,
	pub level: Level,
	pub msg: String,
	pub context: Context,
}

impl Record {
	pub fn empty() -> Record {
		Record::new(Level::None, "")
	}

	pub fn new<M>(level: Level, msg: M) -> Record
	where
		M: Into<String>,
	{
		Record {
			dt: Utc::now(),
			level: level.clone(),
			msg: msg.into(),
			context: Context::new(),
		}
	}

	pub fn set_context<V: Into<Context>>(&mut self, context: V) -> &mut Self {
		self.context = context.into();
		self
	}

	pub fn with_context<V: Into<Context>>(mut self, context: V) -> Self {
		self.set_context(context.into());
		self
	}

	pub fn set<V>(&mut self, k: &str, v: V) -> &mut Self
	where
		V: Serialize,
	{
		self.context.set(k, v);
		self
	}

	pub fn with<V>(mut self, k: &str, v: V) -> Self
	where
		V: Serialize,
	{
		self.set(k, v);
		self
	}

	pub fn set_error(&mut self, e: &Error) -> &mut Self {
		self.set("error", e.description().to_owned());
		let mut trace = Vec::new();
		let mut e2 = e;
		while let Some(cause) = e2.cause() {
			trace.push(format!("{:?}", cause));
			e2 = cause;
		}

		self.set(
			"error_trace",
			trace,
		);
		self
	}

	pub fn with_error(mut self, e: &Error) -> Self {
		self.set_error(e);
		self
	}

	pub fn merge_context<V: Into<Context>>(&mut self, context: V) -> &mut Self {
		for (k, v) in context.into().iter() {
			if !self.context.contains_key(k) {
				self.context.insert(k.clone(), v.to_owned());
			}
		}
		self
	}

	pub fn override_context<V: Into<Context>>(&mut self, context: V) -> &mut Self {
		for (k, v) in context.into().iter() {
			self.context.insert(k.clone(), v.to_owned());
		}
		self
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_context_merge() {
		let context = Context::new()
			.with("string", "some string")
			.with("bool", true)
			.with("array", vec!["a", "b"]);

		let mut record = Record::new(Level::Error, "message")
			.with("extra", "some extra value")
			.with("string", "a different string");

		record.merge_context(&context);

		assert!(record.context.contains_key("extra"));
		assert_eq!("some extra value", record.context.get("extra").unwrap());

		assert!(record.context.contains_key("string"));
		assert_eq!("a different string", record.context.get("string").unwrap());

		assert!(record.context.contains_key("bool"));
		assert_eq!(true, record.context.get("bool").unwrap().as_bool().unwrap());

		assert!(record.context.contains_key("array"));
		assert_eq!("a", record.context.get("array").unwrap().as_array().unwrap()[0]);
		assert_eq!("b", record.context.get("array").unwrap().as_array().unwrap()[1]);
	}
}
