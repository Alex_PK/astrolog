/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#[macro_use]
extern crate lazy_static;

use std::error::Error;

use serde::Serialize;
use std::sync::RwLock;

pub mod errors;
pub mod formatter;
pub mod handler;
pub mod logger;

pub use crate::logger::{Context, Level, Logger, Record};

pub mod prelude {
	pub use crate::formatter::{ContextFormatting, DateFormat, DateFormatting, LevelFormat, LevelFormatting};
	pub use crate::formatter::{HtmlFormatter, JsonFormatter, LineFormatter};
	pub use crate::handler::LevelAware;
	pub use crate::logger::{Level, Logger};
}

pub mod handler_prelude {
	pub use crate::errors::{HandlerError, HandlerResult};
	pub use crate::formatter::*;
	pub use crate::handler::{Handler, LevelAware};
	pub use crate::logger::{Context, Level, Record};
}

lazy_static! {
	static ref ASTROLOG_GLOBAL: RwLock<Logger> = RwLock::new(Logger::new());
}

pub fn reset_global_logger() {
	let mut global_logger = ASTROLOG_GLOBAL.write().unwrap();
	(*global_logger) = Logger::new();
}

pub fn config<F>(f: F)
where
	F: FnOnce(&mut Logger),
{
	let mut l = ASTROLOG_GLOBAL.write().unwrap();
	f(&mut l);
}

pub fn with<V>(k: &str, v: V) -> logger::Item
where
	V: Serialize,
{
	logger::Item::new_global().with(k, v)
}

pub fn with_error(e: &Error) -> logger::Item {
	logger::Item::new_global().with_error(e)
}

pub fn with_new_context<V:Into<Context>>(ctx: V) -> logger::Item<'static> {
	logger::Item::new_global().with_new_context(ctx)
}

pub fn log<S: ToString>(level: Level, msg: S) {
	ASTROLOG_GLOBAL.read().unwrap().log(level, msg);
}

pub fn trace<S: ToString>(msg: S) {
	log(Level::Trace, msg);
}
pub fn profile<S: ToString>(msg: S) {
	log(Level::Profile, msg);
}
pub fn debug<S: ToString>(msg: S) {
	log(Level::Debug, msg);
}
pub fn info<S: ToString>(msg: S) {
	log(Level::Info, msg);
}
pub fn notice<S: ToString>(msg: S) {
	log(Level::Notice, msg);
}
pub fn warning<S: ToString>(msg: S) {
	log(Level::Warning, msg);
}
pub fn error<S: ToString>(msg: S) {
	log(Level::Error, msg);
}
pub fn critical<S: ToString>(msg: S) {
	log(Level::Critical, msg);
}
pub fn alert<S: ToString>(msg: S) {
	log(Level::Alert, msg);
}
pub fn emergency<S: ToString>(msg: S) {
	log(Level::Emergency, msg);
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::thread;

	#[test]
	fn multi_thread() {
		use super::handler::vec::VecHandler;
		let handler = VecHandler::new();
		let store = handler.get_store();

		config(|l| {l.push_handler(handler);});

		let th = thread::spawn(|| {
			debug("Thread debug message");
		});

		warning("Main warning message");

		let _ = th.join();

		let logs = store.take();

		assert!(logs.iter().position(|v| v.msg.contains("Thread debug")).is_some());
		assert!(logs.iter().position(|v| v.msg.contains("Main warning")).is_some());
	}
}
