/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use std::error::Error;
use std::fmt;

pub type HandlerResult = Result<bool, HandlerError>;

#[derive(Debug)]
pub struct HandlerError {
	msg: String,
	cause: Option<Box<Error>>,
}

impl HandlerError {
	pub fn new(msg: &str) -> Self {
		HandlerError {
			msg: msg.to_owned(),
			cause: None,
		}
	}

	pub fn context<E>(e: E, msg: &str) -> Self
	where
		E: Error + 'static,
	{
		HandlerError {
			msg: msg.to_owned(),
			cause: Some(Box::new(e)),
		}
	}

	pub fn wrap<E>(msg: &str) -> impl FnOnce(E) -> HandlerError
	where
		E: Error + 'static,
	{
		let msg = msg.to_owned();
		move |e: E| HandlerError {
			msg: msg,
			cause: Some(Box::new(e)),
		}
	}
}

impl Error for HandlerError {
	fn description(&self) -> &str {
		&self.msg
	}

	fn cause(&self) -> Option<&Error> {
		if let &Some(ref e) = &self.cause {
			return Some(e.as_ref());
		}
		None
	}
}

impl fmt::Display for HandlerError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{}", self.description())
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_basic_error() {
		fn test_fn() -> Result<String, HandlerError> {
			Err(HandlerError::new("Some error"))
		}

		assert!(test_fn().is_err())
	}

	#[test]
	fn test_error_context() {
		use std::io;
		fn test_fn() -> Result<String, HandlerError> {
			let e = io::Error::new(io::ErrorKind::NotFound, "Something");
			Err(HandlerError::context(e, "Some error"))
		}

		let res = test_fn();
		assert!(res.is_err());
	}

	#[test]
	fn test_error_wrapper() {
		use std::io;
		fn test_fn() -> Result<String, io::Error> {
			Err(io::Error::new(io::ErrorKind::NotFound, "Something"))
		}

		let res = test_fn().map_err(HandlerError::wrap("some error"));
		assert!(res.is_err());
	}
}
