/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

extern crate astrolog;

#[cfg(test)]
mod tests {
	use astrolog::formatter::context::{Formatter, Json5};
	use astrolog::*;
	use serde::Serialize;

	#[test]
	fn serializable_in_context() {
		#[derive(Serialize)]
		struct Data {
			pub v1: String,
			pub v2: i32,
			pub v3: Vec<bool>,
		}

		let d = Data {
			v1: "Test".into(),
			v2: 123,
			v3: vec![true, false, true],
		};

		let ctx = Context::new().with("data", d);
		let rdr = Json5 {};

		assert_eq!(
			"{data: {v1: \"Test\", v2: 123, v3: [true, false, true]}}",
			rdr.render(&ctx)
		);
	}
}
