/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

extern crate astrolog;

use std::env;
use std::rc::Rc;

use astrolog::handler::console::ConsoleHandler;
use astrolog::prelude::*;

fn main() {
	let logger = Logger::new()
		.with_global("OS", env::consts::OS)
		.with_handler(ConsoleHandler::new().with_levels_range(Level::Info, Level::Emergency))
		.into_rc();

	get_by_ref(&logger);
	get_by_rc(logger.clone());
}

fn get_by_ref(logger: &Logger) {
	logger.info("Passed by reference")
}

fn get_by_rc(logger: Rc<Logger>) {
	logger.info("Passed by Rc")
}
