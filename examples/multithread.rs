/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

extern crate astrolog;

use std::env;
use std::thread;

use astrolog::handler::console::ConsoleHandler;
use astrolog::prelude::*;

fn main() {
	let logger = Logger::new()
		.with_global("OS", env::consts::OS)
		.with_handler(ConsoleHandler::new().with_levels_range(Level::Info, Level::Emergency))
		.into_arc();

	let ths = (0..10)
		.map(|i| {
			let logger = logger.clone();
			thread::spawn(move || {
				logger
					.with("line", line!())
					.with("file", file!())
					.with("thread", i)
					.info("A simple info logging");

				if i % 2 == 0 {
					logger.with("thread", i).emergency("This thread did something bad");
				}
			})
		})
		.collect::<Vec<_>>();

	for t in ths {
		t.join().expect("Thread join failed");
	}
}
