/*
Astrolog, a logging framework for Rust
Copyright (C) 2019 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

extern crate astrolog;

use std::env;

use serde_json::json;

use astrolog::handler::console::ConsoleHandler;
use astrolog::prelude::*;

fn main() {
	let logger = Logger::new().with_handler(ConsoleHandler::new().with_levels_range(Level::Info, Level::Emergency));

	logger.info("With no context");

	let logger = logger.with_global("OS", env::consts::OS);

	logger.debug("A debug message"); // Will not be printed
	logger.info("A simple info logging");

	logger
		.with("line", line!())
		.with("file", file!())
		.error("An error with some debug info");

	logger
		.with_multi(json!({
			"line": line!(),
			"file": file!(),
		}))
		.critical("This is dangerous!");

	let logger = Logger::new()
		.with_handler(ConsoleHandler::new()
			.with_formatter(LineFormatter::new()
				.with_date_format(DateFormat::Email)
				.with_level_format(LevelFormat::LowerShort)
			)
		);

	logger
		.with("line", line!())
		.with("file", file!())
		.error("An error with custom formatting");

}
